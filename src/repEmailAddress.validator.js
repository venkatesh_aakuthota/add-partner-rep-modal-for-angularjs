import SessionManager from 'session-manager';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';

export default function repEmailAddressValidator($q,
                                                 partnerRepServiceSdk:PartnerRepServiceSdk,
                                                 sessionManager:SessionManager) {
    return {
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.repEmailAddress = function (modelValue, viewValue) {

                return $q((resolve, reject)=> {
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                partnerRepServiceSdk
                                    .searchForPartnerRepWithEmailAddress(
                                        viewValue,
                                        accessToken
                                    )
                                    .then(partnerReps => {
                                            if (!partnerReps) {
                                                resolve(true);
                                            }
                                            else {
                                                reject();
                                            }
                                        }
                                    )
                        );

                });
            };
        }
    };
}


