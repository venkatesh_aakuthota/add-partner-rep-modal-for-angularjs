import angular from 'angular';
import '../src/module';
import RouteConfig from './routeConfig';
import SessionManager,{SessionManagerConfig} from 'session-manager';
import PartnerRepServiceSdk,{PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';
import 'angular-route';

const precorConnectApiBaseUrl = 'https://api-dev.precorconnect.com';

const sessionManager =
    new SessionManager(
        new SessionManagerConfig(
            precorConnectApiBaseUrl/* identityServiceBaseUrl */,
            'https://precor.oktapreview.com/app/template_saml_2_0/exk5cmdj3pY2eT5JU0h7/sso/saml'/* loginUrl */,
            'https://dev.precorconnect.com/customer/account/logout/'/* logoutUrl*/,
            30000/* accessTokenRefreshInterval */
        )
    );


const partnerRepServiceSdk =
    new PartnerRepServiceSdk(
        new PartnerRepServiceSdkConfig(
            precorConnectApiBaseUrl
        )
    );

angular
    .module(
        'exampleApp.module',
        [
            'ngRoute',
            'addPartnerRepModal.module'
        ]
    )
    .constant(
        'sessionManager',
        sessionManager
    )
    .constant(
        'partnerRepServiceSdk',
        partnerRepServiceSdk
    )
    .config(
        [
            '$routeProvider',
            $routeProvider => new RouteConfig($routeProvider)
        ]
    );
